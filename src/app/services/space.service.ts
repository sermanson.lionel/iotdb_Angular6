import { Injectable } from '@angular/core';
import { Space } from '../model/space';
import { IdgeneratorService } from './idgenerator.service';

@Injectable({
  providedIn: 'root'
})
export class SpaceService {

  // private space: Space;
  // there's NOT ONLY ONE SPACE !
  constructor(private idGenerator: IdgeneratorService) { }
  
  getSpace(spaceId) {
    return new Space(
      'blank Space generated in SpaceService',
      'Some Userid of size 15',
      this.idGenerator.generateRandomId('CHARS_AND_NUMBERS', 15)
    );
  }

  editSpace(space: Space) {

    return new Promise((resolve, reject) => {
      reject('test');
    });
  }

}
