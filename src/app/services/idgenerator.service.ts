
import { Injectable } from '@angular/core';
// enum typeSelector { CHARS, NUMBERS, CHARS_AND_NUMBERS }


@Injectable({
  providedIn: 'root'
})
export class IdgeneratorService {

  private chars: string = 'abcdefghijklmnopqrstuvwwyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  private numbers: string = '0123456789';
  constructor() { }
  
  generateRandomId( contentType: string, idSize: number ): string {
    let newId: string = '';
    let dataPicker: string = '';
    switch (contentType) {
      case 'CHARS':
        dataPicker = this.chars; break;
      case 'CHARS_AND_NUMBERS':
        dataPicker = this.chars + this.numbers; break;
      case 'NUMBERS':
        dataPicker = this.numbers; break;
      default: break;
    }

    for (let i = 0; i < idSize; i++) {
      newId = newId + dataPicker[ Math.floor( Math.random() * (dataPicker.length - 2) ) ] // random char between 0 and dataPicker's size -1
    }
    return newId;
  }
}
