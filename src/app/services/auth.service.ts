import { Injectable, Inject, OnInit } from '@angular/core';
import { IdgeneratorService } from './idgenerator.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnInit {

  private authUser = null;
  private authUserSession = sessionStorage;
  constructor(
    private idGenerator: IdgeneratorService,
    @Inject('baseWebcomref') private webcomRef
  ) { }

  ngOnInit() {

    // this.webcomRef.resume(this.checkAuth); // Resumes the authentication state (based on records from the local storage).
    this.webcomRef.registerAuthCallback(this.checkAuth);
    // FIXME doesn't work! registerAuthCallback is never called, no matter user's auth status.
  }
  checkAuth(error, auth) {
    if (error == null) {
      if (auth == null) {
        console.log('Not connected');
        this.logOut();
      } else {
        console.log('Authenticated: ' + auth.providerUid);
        const requestedLocation = localStorage.getItem('requestedLocation');
        console.log('requestedLocation', requestedLocation);
        this.authUser = auth;
        // refreshDatas(); // tf it does
      }
    } else {
      console.log('Error auth: ' + error);
    }
  }

  /*
  registerAuthCallback(error, auth) {
    if (error) {
      console.log('registerAuthCallback: An error occurred while authenticating', error);
    } else if (auth) {
      console.log('registerAuthCallback: The user with the following identity information has logged in', auth);
    } else {
      console.log('registerAuthCallback: The user has logged out');
    }
  }
  */

  /** Login function **/
  login(email, password) {

    const credentials = {
      email: email,
      password: password
    };
    console.log('try to log in with ', credentials);

    return new Promise((resolve, reject) => {
      this.webcomRef.authWithPassword(credentials)
        .then((success) => {
          console.log('authservice login: success==', success);

          this.authUserSession.setItem('userUid', success.uid);
          this.authUserSession.setItem('userEmail', success.providerUid);
          this.authUserSession.setItem('userExpires', success.expires);
          this.authUserSession.setItem('userPass', credentials.password);

          if (!this.isEmpty(success.providerProfile)) { // if there's some additional user's info
            this.authUserSession.setItem('hasAdditionalData', 'true');
            const additionnalData = success.providerProfile;
            for (const key in additionnalData) {
              this.authUserSession.setItem(key, additionnalData[key]);
            }
          }

          this.webcomRef.registerAuthCallback(this.checkAuth);
          this.authUser = success;
          resolve(success);
        })
        .catch((error) => {
          console.log(error);
          console.log('error.code in login fn (auth-service)' + error.code);
          reject(error.code);
          // refreshDatas();
        });
    });
  }

  isEmpty(obj: Object) {

    for (const key in obj) {
      if (obj.hasOwnProperty(key)) { return false; } // This object is NOT empty.
    }
    return true; // This object is empty.
  }
  /** userCreation function **/
  createUser(email, password) {

    const credentials = {
      email: email,
      password: password
    };

    return new Promise((resolve, reject) => {
      this.webcomRef.createUser(credentials, (error, auth) => {
        if (error) {
          switch (error.code) {
            case 'EXISTING_IDENTITY':
              reject('Email already in use!'); break;
            case 'INVALID_EMAIL':
              reject('Email format is not valid!'); break;
            default:
              reject('Account creation failed: ' + error.message);
          }
        } else {
          /// generate a new random number id allowing the user to manage space.
          const newUserSpaceRef = this.idGenerator.generateRandomId('CHARS_AND_NUMBERS', 15);
          // tslint:disable-next-line:no-shadowed-variable
          this.webcomRef.updateEmailPasswordProfile({ 'spaceRef': newUserSpaceRef }, (error) => {
            if (error) {
              console.log('WARNING: could not create user reference used for space managing. error: ', error);
            } else {
              /*
              this.authUserSession.setItem('userSpaceRef', newUserSpaceRef.toString());
              */
            }
          });
          /*
          this.authUserSession.setItem('userUid', auth.uid);
          this.authUserSession.setItem('userEmail', auth.providerUid);
          this.authUserSession.setItem('userExpires', auth.expires);
          this.authUserSession.setItem('userPass', credentials.password);
          */
          resolve('Successfully created account with UID:' + auth.uid + ', it must be confirmed before use!');
        }
      });
    });
  }

  /** Disconnect function **/
  logOut() {
    return new Promise((resolve, reject) => {
      this.webcomRef.logout((error) => {
        if (error) {
          // refreshDatas();
          console.log('an error occurred in disconnect fn (auth-service)', error.code);
          reject(error.code);
        } else {

          this.authUser = null;
          console.log(this.authUserSession.getItem('userMail'), 'Disconnection succeeded');
          this.authUserSession.clear();

          resolve('true');
        }
      });
    });
  }

  /** Get the user authentication state function **/
  getAuth() {
    this.webcomRef.unregisterAuthCallback(); // NOTICEME why doing that???
    this.webcomRef.registerAuthCallback(this.checkAuth); // NOTICEME Triggers checkauth ?

    if (!!this.authUserSession.getItem('userUid')) {

      // check user's expiration
      // NOTICEME already done by checkauth ?
      const expiration: Number = parseInt(this.authUserSession.getItem('userExpires'));
      if (expiration <= Date.now() / 1000) {
        console.log('getAuth: User\'s session has expired.\nnow===', Date.now() / 1000, '\n expiration===', expiration);

        this.logOut();
        return false;
      }

      // return user's data
      let userData = {
        'userUid': this.authUserSession.getItem('userUid'),
        'userEmail': this.authUserSession.getItem('userEmail'),
        'userExpires': this.authUserSession.getItem('userExpires'),
        'userPass': this.authUserSession.getItem('userPass'),
        'userName': this.authUserSession.getItem('userName'),
        'userSurname': this.authUserSession.getItem('userSurname') || '',
        'userPrefLanguage': this.authUserSession.getItem('userPrefLanguage') || '',
        'userSpaceRef': this.authUserSession.getItem('userSpaceRef') || ''
      };
      return userData;
    }
    return false;
  }

  /** Get the user UID function **/
  getMyUid() {
    this.webcomRef.unregisterAuthCallback();
    this.webcomRef.registerAuthCallback(this.checkAuth);
    /*
    if (!this.authUser) { return this.userId; }
    else { return this.authUser.uid; }
    */
    if (!!this.getAuth()) {
      return this.authUserSession.getItem('userUid');
    }
    return null;
  }

  /** Get the user email function **/
  getMyEmail() {
    this.webcomRef.unregisterAuthCallback();
    this.webcomRef.registerAuthCallback(this.checkAuth);
    /*
    if (!this.authUser) { return null; }
    else { return this.authUser.providerUid; }
    */
    if (!!this.getAuth()) {
      return this.authUserSession.getItem('userEmail');
    }
    return null;
  }

  /**
   * Reset password:
   * Send an email to the authentificated user.
   * @param email : string
   * @returns Promise<'true'>
   */
  resetPwd(email: string): Promise<string> {
    this.webcomRef.unregisterAuthCallback();
    this.webcomRef.registerAuthCallback(this.checkAuth);

    return new Promise((resolve, reject) => {
      this.webcomRef.sendPasswordResetEmail(email, (error) => {
        if (error) { reject(error); } else { resolve('true'); }
      });
    });
  }

  /**
   * Updates users profile with newData object. If the app knows locally that the user have additionnal data,
   * then it considers that these data are in the newData object. It takes them in the newData object.
   * @param newData : userInfo {'name', 'surname', 'prefLanguage'} [OR {'Ids of user's spaces'}(TODO)]
   * @return Promise<string> : the result of the action
   * TODO / NOTICEME Why do we need to explicitly save in local storage ? Doesn't Webcom already do this job ?
   */
  editUser(newData: any): Promise<string> {
    this.webcomRef.unregisterAuthCallback();
    this.webcomRef.registerAuthCallback(this.checkAuth);

    return new Promise((resolve, reject) => {
      if (!!this.getAuth() === false) { reject('not connected.'); }

      if (!!this.authUserSession.getItem('hasAdditionalData')) {
        if (newData['userName']) {
          this.authUserSession.setItem('userName', newData['userName']);
        } else {
          newData['userName'] = this.authUserSession.getItem('userName');
        }
        if (newData['userSurname']) {
          this.authUserSession.setItem('userSurname', newData['userSurname']);
        } else {
          newData['userSurname'] = this.authUserSession.getItem('userSurname');
        }
        if (newData['userPrefLanguage']) {
          this.authUserSession.setItem('userPrefLanguage', newData['userPrefLanguage']);
        } else {
          newData['userPrefLanguage'] = this.authUserSession.getItem('userPrefLanguage');
        }
        if (newData['userSpaceRef']) {
          this.authUserSession.setItem('userSpaceRef', newData['userSpaceRef']);
        } else {
          newData['userSpaceRef'] = this.authUserSession.getItem('userSpaceRef');
        }
      }
      this.webcomRef.updateEmailPasswordProfile(newData, (error) => {
        if (error) { reject(error); }
        else {
          resolve(newData);
        }
      });
    });
  }

  /**
   * Edit the user's personal reference to a space.
   * Checks if a reference already exists locally.
   * If user's space reference is defined, we cant modify it. Otherwise the user wouldn't manage spaces.
   * If not so, it creates a new id reference and saves it in local storage.
   * Calls editUser() to save in local storage.
   */
  editUserSpaceRef(): Promise<string> {
    this.webcomRef.unregisterAuthCallback();
    this.webcomRef.registerAuthCallback(this.checkAuth);

    if (!!this.getAuth() === false) { return Promise.reject('not connected.'); }

    let userSpaceRef = this.authUserSession.getItem('userSpaceRef');
    // resolve(!!userSpaceRef);

    // if user's space reference is defined, we cant modify it. Otherwise the user wouldn't manage spaces.
    if (!!userSpaceRef === false) { // if it is NOT defined
      userSpaceRef = this.idGenerator.generateRandomId('CHARS_AND_NUMBERS', 20);
      return this.editUser({'userSpaceRef': userSpaceRef});
    } else { return Promise.reject('User\'s space reference already defined.'); }
  }
  /**
   * TODO Do JSdocs !
   * TODO finish the spaces job !
   * @param newSpaceId : number
   * @returns Promise<'true'>
   */
  addSpaceToUser(newSpaceId: number): Promise<string> {
    this.webcomRef.unregisterAuthCallback();
    this.webcomRef.registerAuthCallback(this.checkAuth);

    return new Promise((resolve, reject) => {
      if (!!this.getAuth() === false) { reject('not connected.'); }

      // data of user's spaces are provided in a string in sessionStorage.
      const userSpacesIds = JSON.parse(this.authUserSession.getItem('userSpacesIds')) || {};
      userSpacesIds[newSpaceId] = true;


      this.webcomRef.updateEmailPasswordProfile(userSpacesIds, (error) => {
        if (error) { reject(error); } else {
          this.authUserSession.setItem('userSpaceIds', JSON.stringify(userSpacesIds));
          // we need to store the data of user's spaces in a string form in sessionStorage.

          this.authUserSession.setItem('userRequestedPage', 'sensor/' + newSpaceId);
          resolve('true');
        }
      });
    });
  }

  /**
   * @returns Promise< {'userSpaceId1': true, 'userSpaceId2': true, ...} >
   */
  getSpacesIds(): Promise<Object> {
    this.webcomRef.unregisterAuthCallback();
    this.webcomRef.registerAuthCallback(this.checkAuth);

    return new Promise((resolve, reject) => {
      if (!!this.getAuth() === false) { reject('not connected.'); }

      // if the user reference for spaces is NOT defined, then the user never registered new spaces.
      if (this.authUserSession.getItem('userSpaceRef') !== '') { reject('No user space reference.'); } else {
        resolve(JSON.parse(this.authUserSession.getItem('userSpacesIds')));
      }
    });
  }

  getUserSpaceRef() {
    this.webcomRef.unregisterAuthCallback();
    this.webcomRef.registerAuthCallback(this.checkAuth);

    return new Promise((resolve, reject) => {
      if (!!this.getAuth() === false) { reject('not connected.'); }
      if (this.authUserSession.getItem('userSpaceRef') !== '') { reject('No user space reference.'); }
      resolve(this.authUserSession.getItem('userSpaceRef'));
    });
  }

  getUserRequestedPage() {
    this.webcomRef.unregisterAuthCallback();
    this.webcomRef.registerAuthCallback(this.checkAuth); // TODO / NOTICEME we need to add a ".then" if exists so that we decide to continue or not.

    return new Promise((resolve, reject) => {
      if (!!this.getAuth() === false) { resolve('login'); }
      if (!this.authUserSession.getItem('userRequestedPage')) { resolve('useredit'); }
      resolve(this.authUserSession.getItem('userRequestedPage'));
    });
  }
  setUserRequestedPage(newRequPage: string) {
    this.webcomRef.unregisterAuthCallback();
    this.webcomRef.registerAuthCallback(this.checkAuth);

    return this.authUserSession.setItem('userRequestedPage', newRequPage);
  }

}
