export class User {
    public name: string;
    public surname: string;
    public mail: string;
    public password: string;
    public userid: string;
    public spacesid: string[];
    public prefLanguage: string;
    public spaceRef: string;
    constructor( newName, newSurname, newMail, newPass, newId, newSpacesId?, newPrefLang?, newSpaceRef? ) {
        this.name = newName;
        this.surname = newSurname;
        this.mail = newMail;
        this.password = newPass;
        this.userid = newId;
        this.spacesid = newSpacesId || [];
        this.prefLanguage = newPrefLang || '';
        this.spaceRef = newSpaceRef || '';
    }
    /*
    constructor(
        private _name: string,
        private _surname: string,
        private _mail: string,
        private _password: string,
        private _userid: string
    ) {}
    get name(): string { return this._name; }
    get surname(): string { return this._surname; }
    get mail(): string { return this._mail; }
    get password(): string { return this._password; }
    get userid(): string { return this._userid; }
    set name(newName: string) {  this._name = newName; }
    set surname(newSurname: string) {  this._surname = newSurname; }
    set mail(newMail: string) {  this._mail = newMail; }
    set password(newPass: string) {  this._password = newPass; }
    set userid(newId: string) {  this._userid = newId; }
    */
}
