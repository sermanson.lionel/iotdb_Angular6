export class Space {
    public spaceName: string;
    public id: string;
    public description: string;
    public ownerId: string;
    public collaborators: Object;
    constructor( newId, newOwnerId, newName?, newDesc?, newCollaborators? ) {
        this.id = newId;
        this.ownerId = newOwnerId;
        this.spaceName = newName || '';
        this.description = newDesc || '';
        this.collaborators = newCollaborators || {};
    }

}
