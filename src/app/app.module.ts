import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes, Router } from '@angular/router';

import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { UsereditComponent } from './useredit/useredit.component';
import { SpaceeditionComponent } from './spaceedition/spaceedition.component';
import { SensorsComponent } from './sensors/sensors.component';
import { availableLangs, availableSpacesRules } from './availables/availables';

// import * as Webcom from 'webcom/webcom';
import Webcom from 'webcom/webcom'; // this one works
import { AuthGuard } from './guards/auth.guard';
import { HeadComponent } from './head/head.component';

// import { CommonModule } from '@angular/common';

// default routes
const routes: Routes = [
  // { path: '', component: AppComponent }, // it makes two app components!
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'useredit', component: UsereditComponent, canActivate: [AuthGuard] },
  { path: 'spaceedition', component: SpaceeditionComponent, canActivate: [AuthGuard] },
  { path: 'spaceedition/:action', component: SpaceeditionComponent, canActivate: [AuthGuard] },
  { path: 'sensors', component: SensorsComponent, canActivate: [AuthGuard] }
];


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    UsereditComponent,
    SpaceeditionComponent,
    SensorsComponent,
    HeadComponent
  ],
  imports: [
    // CommonModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    { provide: 'baseWebcomref', useValue: new Webcom('https://io.datasync.orange.com/base/iotdb') },
    { provide: 'available_langs', useValue: availableLangs },
    { provide: 'available_spaces_rules', useValue: availableSpacesRules },
    { provide: 'webcom_img_url', useValue: 'assets/img/iot-dashboard-webcom-logo.png' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
