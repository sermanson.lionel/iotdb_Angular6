import { Component, OnInit, Inject } from '@angular/core';
import { User } from '../model/user';
import { SpaceService } from '../services/space.service';
import { AuthService } from '../services/auth.service';
import { Space } from '../model/space';
import { Router } from '@angular/router';

@Component({
  selector: 'app-useredit',
  templateUrl: './useredit.component.html',
  styleUrls: ['./useredit.component.scss']
})
export class UsereditComponent implements OnInit {

  private localUserModel: User;
  private userSpaces: Space[] = [
    new Space('', '', '')
  ];
  private newSpaceModel: Space;
  private userIdToInvite = '';
  constructor(
    private router: Router,
    private spaceService: SpaceService,
    private authService: AuthService,
    @Inject('available_langs') private availableLanguages
  ) { }

  ngOnInit() {
    const userInfo = this.authService.getAuth() || {
      'userUid': '',
      'userEmail': '',
      'userExpires': '',
      'userPass': '',
      'userName': '',
      'userSurname': '',
      'userPrefLanguage': '',
      'userSpaceRef': '',
    };
    this.localUserModel = new User(
      userInfo.userName,
      userInfo.userSurname,
      userInfo.userEmail,
      userInfo.userPass,
      userInfo.userUid,
      [],
      userInfo.userPrefLanguage,
      userInfo.userSpaceRef
    );
    this.localUserModel.spacesid.forEach((spaceId) => {
      this.userSpaces.push(this.spaceService.getSpace(spaceId));
    });

    this.newSpaceModel = new Space('', '', '');
  }

  editOnSubmit() {
    const userInfo = {
      'userName': this.localUserModel.name,
      'userSurname': this.localUserModel.surname,
      'userPrefLanguage': this.localUserModel.prefLanguage
    };

    this.authService.editUser(userInfo)
      .then((result) => {
        console.log('useredit: Editing user successed. result==', result);
      })
      .catch((error) => {
        console.log('useredit: error while editing user: ', error);
      });
  }

  editPasswordOnSubmit() {

    if (confirm('An email will be sended to you. Confirm?')) {
      this.authService.resetPwd(this.localUserModel.password)
        .then((result) => {
          console.log('mail sended.', result);
        })
        .catch((error) => {
          console.log('useredit: Error while resetting password. ', error);
        });
    }
  }

  editUserSpaceRef() {
    if (this.localUserModel.spaceRef === '') {
      this.authService.editUserSpaceRef()
        .then((result) => {
          console.log('edit user\'s space reference edition successed. ', result);
          this.localUserModel.spaceRef = result['userSpaceRef'];
        })
        .catch((error) => {
          console.log('useredit: editUserSpaceRef error: ', error);
        });
    } else { alert('Editing your space reference would prevent you from manage the spaces you\'ve contributed.'); }
  }

  requestInvitation() {
    // TODO
  }

  goToSpace(spaceId: string) {
    // this.router.navigate(spaceId)
  }
}
