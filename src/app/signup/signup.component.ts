import { Component, OnInit, Inject } from '@angular/core';
import { User } from '../model/user';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  private tempUser: User;
  private repeatedPassword: string;
  constructor(
    @Inject('webcom_img_url') private webcomImgUrl,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.tempUser = new User('', '', '', '', '');
    this.repeatedPassword = '';
  }

  signupOnSubmit() {
    if (this.tempUser.mail !== '' && this.tempUser.password !== '') {

      if (this.tempUser.password === this.repeatedPassword) {
        this.authService.createUser(this.tempUser.mail, this.tempUser.password)
        .then((succesMsg) => {
          console.log('signup: ', succesMsg, '. Routing to login page...');
          this.router.navigate(['/login']);
          alert('Your account have been saved succefully.\nA mail confirmation is required to use the application.');
        })
        .catch((errorMsg) => {
          alert(errorMsg);
          this.tempUser.password = '';
          this.repeatedPassword = '';
        });
      } else {
        alert('Passwords does not match.');
        this.tempUser.password = '';
        this.repeatedPassword = '';
      }
    } else {
      console.log('tempUser===', this.tempUser, '\nplease enter valid credentials.');
    }
  }
}
