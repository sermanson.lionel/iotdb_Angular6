import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-head',
  templateUrl: './head.component.html',
  styleUrls: ['./head.component.scss']
})
export class HeadComponent implements OnInit {

  private userSpacesId = [];
  constructor(
    private authService: AuthService,
    @Inject('webcom_img_url') private webcomImgUrl,
    private router: Router
  ) { }

  ngOnInit() {
    this.authService.getSpacesIds()
      .then((spaceIdsObj) => {
        for (let spaceId in spaceIdsObj) {
          this.userSpacesId.push(spaceId);
        }
      })
      .catch();
  }

  logOut() {
    this.authService.logOut()
      .then(() => { this.router.navigate(['/login']); })
      .catch((errorMsg) => { console.log('headcomponent: ',errorMsg); });
  }
}
