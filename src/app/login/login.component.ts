import { Component, OnInit, Inject } from '@angular/core';
import { User } from '../model/user';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private tempUser: User = new User('', '', '', '', '');
  constructor(
    @Inject('webcom_img_url') private webcomImgUrl,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    if (this.authService.getAuth()) {
      console.log('login: user already connected. routing to user edit...');
      this.router.navigate(['/useredit']);
    } else {
      console.log('not connected yet.');
    }
  }

  loginOnSubmit() {

    if (this.authService.getAuth()) {
      console.log('login: user already connected. routing to user edit...');

      this.router.navigate(['/useredit']);
    } else {
      this.authService.login( this.tempUser.mail, this.tempUser.password )
        .then(() => {
          console.log('login: Connection successed.');
          console.log('login: checkauth===', this.authService.getAuth());
          console.log('login: Routing to requestedPage...');

          this.authService.getUserRequestedPage()
            .then((requestedRoute) => {
              this.router.navigate(['/' + requestedRoute]);
            });
        })
        .catch((errorCode) => {
        alert('error: ' + errorCode);
          this.tempUser.password = '';
      });
    }
  }
}
