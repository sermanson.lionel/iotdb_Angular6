import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router
  ) { }
  /*
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return true;
  }
  */
  canActivate(route: ActivatedRouteSnapshot): boolean | UrlTree {
    const requestedRoute = route.url.toString();
    console.log("route.url.tostring===", requestedRoute);
    this.authService.setUserRequestedPage(requestedRoute); // Because it's the auth service that manages the localstorage.
    
    if (!!this.authService.getAuth()) {
      console.log('canactivate: user connected.');

      return true;
    } else {
      console.log('canactivate: user NOT connected.');
      return this.router.parseUrl('/login');
    }
  }
}
