import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Space } from '../model/space';
import { IdgeneratorService } from '../services/idgenerator.service';
import { AuthService } from '../services/auth.service';
import { SpaceService } from '../services/space.service';

@Component({
  selector: 'app-spaceedition',
  templateUrl: './spaceedition.component.html',
  styleUrls: ['./spaceedition.component.scss']
})
export class SpaceeditionComponent implements OnInit {

  private newSpace: boolean;
  private localSpaceModel: Space;
  private newCollaborator = {'id': '', 'rule': 'Member'};
  constructor(
    private route: ActivatedRoute,
    private idGenerator: IdgeneratorService,
    private authService: AuthService,
    private spaceService: SpaceService,
    @Inject('available_spaces_rules') private availableRules
  ) { }

  /**
   * params == "new" OR a space id
   */
  ngOnInit() {
    this.route.params.subscribe((params: Params) => {

      if (!params.action || params.action === 'new') {
        this.localSpaceModel = new Space(
          this.idGenerator.generateRandomId('CHARS_AND_NUMBERS', 15),
          this.authService.getUserSpaceRef()
        );
      } else {
        this.localSpaceModel = this.spaceService.getSpace(params.action);
      }
    });
  }

  editOnSubmit() {

    // TODO if userID !== space.ownerId : return error.
    this.spaceService.editSpace(this.localSpaceModel)
      .then(() => {
        console.log('spaceedition editSpaceOnSubmit: editing of space successed.');
      })
      .catch((error) => {
        console.log('editSpaceOnSubmit: error:', error);
      });
  }

  getCollaboratorsIds(): string[] {

    // const collabs = this.localSpaceModel.collaborators;
    // let collabTab = [];
    // for (let id in collabs) {
    //   if (!collabs.hasOwnProperty(id)) return collabTab; // collabs is empty / there's no collabs
    //   collabTab.push(id);
    // }
    // return collabTab;
    return Object.keys(this.localSpaceModel.collaborators);
  }

  addCollaboratorOnSubmit() {

    this.localSpaceModel.collaborators[this.newCollaborator.id] = true;
    this.spaceService.editSpace(this.localSpaceModel)
      .then(() => {
        console.log('spaceedition addCollaboratorOnSubmit: editing of space successed.');
      })
      .catch((error) => {
        console.log('spaceedition addCollaboratorOnSubmit: error: ', error);
      });
  }

  removeCollaborator(collabId) {
    delete this.localSpaceModel.collaborators[collabId];
    this.spaceService.editSpace(this.localSpaceModel)
      .then(() => {
        console.log('spaceedition collaborator is removed succefully.');
      })
      .catch((error) => {
        console.log('spaceedition removeCollaborator: error: ', error);
      });
  }

}
