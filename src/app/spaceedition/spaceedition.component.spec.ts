import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaceeditionComponent } from './spaceedition.component';

describe('SpaceeditionComponent', () => {
  let component: SpaceeditionComponent;
  let fixture: ComponentFixture<SpaceeditionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpaceeditionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaceeditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
