import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { User } from './model/user';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'iotdb-AngularCli';
  user: User;
  constructor(
    private authService: AuthService,
    private router: Router
  ) { }
  ngOnInit() {
    this.authService.getUserRequestedPage()
      .then((requPage) => {
        this.router.navigate([requPage || 'login']);
      })
      .catch((error) => {
        console.log(error);
      });
  }
}
